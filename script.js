// let arr = [12, 123, 44];
// let checkArr = (val) => {
//     return val * 3;
// }
// let result = arr.map(checkArr);
// console.log(result)
// console.log(arr)

// let mas = [1, 2, 3, 4, 5];
//
// let mult = (accumulator, val) => accumulator * val;
//
// let multRes = mas.reduce(mult);
// console.log(multRes)

let mas = [
    {name: 'name 1'},
    {name: 'name 2'},
    {name: 'name 3'},
    {name: 'name 4'},
    {name: 'name 5'},
    {name: 'name 6'},
    {name: 'name 7'}
];

let collectNames = (prev, next) => prev.concat(next.name),
    result = mas.reduce(collectNames, []);
console.log(result);

